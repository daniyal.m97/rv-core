`timescale 1ns / 1ps
module rv_cpu(
	input clk,
	input reset
	);
reg [31:0]prog_mem[0:9];								//program memory
reg [31:0]data_mem[0:10];								//data memory
reg [31:0]register[0:31];								//register file
reg [31:0]pc;
reg [31:0]counter=0;
wire [31:0]instr;
wire [6:0]opcode;
wire [4:0]rd;
wire [4:0]rs1;
wire [4:0]rs2;
wire [11:0]imm;
wire [11:0]b_imm;
wire [11:0]s_imm;
wire [19:0]j_imm;
wire [2:0]func3;
wire [6:0]func7;
wire [4:0]shamt;
wire [31:0]read_reg1;
wire [31:0]read_reg2;
wire [31:0]w_reg_data;
wire [31:0]op1;
wire [31:0]op2;
wire [31:0]out;
wire [1:0]alu_op;
reg [3:0]alu_sel;
wire [31:0]addr;
wire [31:0]w_data;
wire [31:0]r_data;
wire branch;
wire alu_zero;
initial begin
$readmemb("prog.txt",prog_mem);
end
always @ (posedge clk)
begin
   pc<=32'b0;
   counter<=counter+1;
   if(reset) begin
      pc<=32'b0;
   end else if(branch) begin
      pc<=pc+((b_imm[11]==0)?{20'b0,b_imm}:{20'hFFFFF,b_imm});
   end else begin
      pc<=pc+1;
   end
end
assign instr=prog_mem[pc];
//DECODE
assign opcode=instr[6:0];
assign alu_op={opcode[4],opcode[6]};
assign rd=(opcode[5:2]!=4'b1000)?instr[11:7]:rd;
assign rs1=(opcode[2:0]!=3'b111)?instr[19:15]:((opcode[3]==0)?instr[19:15]:rs1);
assign rs2=((opcode[5]==1)&(opcode[2]==0))?instr[24:20]:rs2;
assign func3=(opcode[2:0]!=3'b111)?instr[14:12]:((opcode[3]==0)?instr[14:12]:func3);
assign imm=({opcode[6],opcode[5],opcode[2]}==3'b000)?instr[31:20]:({opcode[6],opcode[5],opcode[2]}==3'b111)?instr[31:20]:imm;
assign b_imm=({opcode[6],opcode[2]}==2'b10)?{instr[31],instr[7],instr[30:25],instr[11:8]}:b_imm;
assign s_imm=(opcode==7'b0100011)?{instr[31:25],instr[11:7]}:s_imm;
assign func7=((alu_op==2'b10)&(opcode[2]==0))?((opcode[5]==1)?instr[31:25]:((func3[1:0]==2'b01)?instr[31:25]:func7)):func7;
assign j_imm=(opcode[2:0]==3'b111)?((alu_op==2'b01)?instr[31:12]:(opcode[3]==0)?j_imm:{instr[31],instr[19:12],instr[20],instr[30:21]}):j_imm;
assign shamt=((alu_op==2'b10)&({opcode[5],opcode[2]}==2'b00)&(func3[1:0]==2'b01))?instr[24:20]:shamt;
always @(*) begin
   if(alu_op==2'b10) begin
   case(func3)
      3'd0: alu_sel<=4'd0;
      3'd1: alu_sel<=4'd1;
      3'd2: alu_sel<=4'd2;
      3'd3: alu_sel<=4'd3;
      3'd4: alu_sel<=4'd4;
      3'd5: alu_sel<=(func7[5]==0)?4'd5:4'd6;
      3'd6: alu_sel<=4'd7;
      3'd7: alu_sel<=4'd8;
   endcase
   end else begin
      alu_sel<=4'd0;
   end
end
//REGISTER
assign read_reg1=(rs1==1'b0)?32'b0:register[rs1];
assign read_reg2=(rs2==1'b0)?32'b0:(opcode[5]==0)?read_reg2:register[rs2];
always @ (posedge clk)
begin
   if(opcode[0]&rd!=0)
      register[rd]<=w_reg_data;
end
//MEMORY
assign w_data=(alu_op==2'b00)?read_reg2:w_data;
assign r_data=(alu_op==2'b00)?data_mem[addr]:r_data;
always @ (*) begin
   if(opcode[5])
      data_mem[addr]<=w_data;
end
//ALU
assign op1=read_reg1;
assign op2=(alu_op==2'b10)?((opcode[5]==0)?((imm[11]==0)?{20'b0,imm}:{20'hFFFFF,imm}):(func7[5]==0)?read_reg2:-read_reg2)
		:((alu_op==2'b00)?((opcode[5]==1)?((s_imm[11]==0)?{20'b0,s_imm}:{20'hFFFFF,s_imm}):op2)
		:((alu_op==2'b01)?-read_reg2:read_reg2));
assign out=(alu_sel==4'd0)?(op1+op2):(alu_sel==4'd1)?(op1<<op2):(alu_sel==4'd2)?($signed(op1)<$signed(op2)?1:0)
		:(alu_sel==4'd3)?(op1<op2?1:0):(alu_sel==4'd4)?(op1^op2):(alu_sel==4'd5)?(op1>>op2):(alu_sel==4'd6)?($signed(op1)>>>op2)
		:(alu_sel==4'd7)?(op1|op2):(alu_sel==4'd8)?(op1&op2):(op1<<12);
assign addr=(alu_op==2'b00)?out:addr;
assign w_reg_data=(alu_op==2'b10)?out:r_data;
//BRANCH
assign alu_zero=(func3[2]==0)?((func3[0])?(out!=0):(out==0)):((func3[0])?(out>=0):(out<0));
assign branch=opcode[6]&alu_zero;
endmodule
