`timescale 1ns / 1ps
module tb;
    reg clk;
    reg reset;
    rv_cpu uut(.clk(clk),.reset(reset));
initial begin
$dumpfile("rv_cpu.vcd");
$dumpvars(0,tb);
reset=1;
#10 reset=0;
end
initial begin
clk=0;
repeat(150) #5 clk=~clk;
end
endmodule
